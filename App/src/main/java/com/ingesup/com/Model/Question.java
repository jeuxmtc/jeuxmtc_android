package com.ingesup.com.Model;

/**
 * Created by Gabriel on 17/12/13.
 */
import java.util.* ;

public class Question {

    public static Question[] questions;


    private int type;
    private ArrayList<Element> fixedElements;
    private ArrayList<Pair> movableElements;
    private String intitule;
    private String explaination ;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ArrayList<Element> getFixedElements() {
        return fixedElements;
    }

    public void setFixedElements(ArrayList<Element> fixedElements) {
        this.fixedElements = fixedElements;
    }

    public ArrayList<Pair> getMovableElements() {
        return movableElements;
    }

    public void setMovableElements(ArrayList<Pair> movableElements) {
        this.movableElements = movableElements;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getExplaination() {
        return explaination;
    }

    public void setExplaination(String explaination) {
        this.explaination = explaination;
    }

    public Question(int type) {
        this.type = type;
        fixedElements = new ArrayList<Element>();
        movableElements = new ArrayList<Pair>();
    }

    public void addFixedElement(Element rep) {
        fixedElements.add(rep);
    }

    public void addMovableElement(int key, Element value) {
        Pair rep = new Pair(key, value);
        movableElements.add(rep);
    }
}

