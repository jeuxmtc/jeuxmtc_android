package com.ingesup.com.Model;

/**
 * Created by Gabriel on 17/12/13.
 */
public class Pair {
    public Pair(int key, Element value) {
        this.key = key;
        this.value = value;
    }

    public int key;
    public Element value;
}
