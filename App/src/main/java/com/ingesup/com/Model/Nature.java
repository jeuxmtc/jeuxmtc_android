package com.ingesup.com.Model;

/**
 * Created by Gabriel on 17/12/13.
 */
public class Nature {
    public int ID;
    public int difficulte ;
    public String libelle ;

    public String aide ;
    public String reference ;

    // Getters et setters
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getAide() {
        return aide;
    }

    public void setAide(String aide) {
        this.aide = aide;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getID() {
        return ID;
    }

    public int getDifficulte() {
        return difficulte;
    }
    public void setDifficulte(int difficulte) {
        this.difficulte = difficulte;
    }
}
