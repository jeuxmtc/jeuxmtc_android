package com.ingesup.com.Model;

/**
 * Created by Gabriel on 17/12/13.
 */
public class Element{

    public int ID;
    public int parent_ID ;
    public int nature1_ID;
    public int nature2_ID;
    public String url_image;

    public String libelle ;
    public String aide ;
    public String reference ;

    // Getters et setters
    public int getID() {
        return ID;
    }

    public int getParent_ID() {
        return parent_ID;
    }
    public void setParent_ID(int parent_ID) {
        this.parent_ID = parent_ID;
    }
    public int getNature1_ID() {
        return nature1_ID;
    }
    public void setNature1_ID(int nature1_ID) {
        this.nature1_ID = nature1_ID;
    }
    public int getNature2_ID() {
        return nature2_ID;
    }
    public void setNature2_ID(int nature2_ID) {
        this.nature2_ID = nature2_ID;
    }
    public String getUrl_image() {
        return url_image;
    }
    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public String getAide() {
        return aide;
    }
    public void setAide(String aide) {
        this.aide = aide;
    }
    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }


}
