package com.ingesup.com;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ingesup.com.Model.*;
import com.ingesup.com.Model.Question;

import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends Activity {

    Chronometer mChronometer;
    public TextView textViewChronometer;
    public TextView textView2;
    public Timer t;
    public int time;
    AlertDialog alertDialog;
    AlertDialog alertDialogValidate;

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.INVISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_game);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        // get the textview chronometer
        textViewChronometer = (TextView)findViewById(R.id.textViewChronometer);
        textView2 = (TextView)findViewById(R.id.textView2);
        int lower = 0;
        int higher = 10;

        int random = (int)(Math.random() * (higher-lower)) + lower;
        textView2.setText((CharSequence) Question.questions[random].getIntitule());
        
        findViewById(R.id.myimage1).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.myimage2).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.myimage3).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.myimage4).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.myimage5).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.layout_earth).setOnDragListener(new MyDragListener());
        findViewById(R.id.layout_fire).setOnDragListener(new MyDragListener());
        findViewById(R.id.layout_metal).setOnDragListener(new MyDragListener());
        findViewById(R.id.layout_water).setOnDragListener(new MyDragListener());
        findViewById(R.id.layout_wood).setOnDragListener(new MyDragListener());
        findViewById(R.id.layout_base_row1).setOnDragListener(new MyDragListener());
        findViewById(R.id.layout_base_row2).setOnDragListener(new MyDragListener());

        startChronometer(20);

        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("you lose !");
        alertDialog.setMessage("Sorry you lose the game because time is out :(");

        alertDialogValidate = new AlertDialog.Builder(this).create();
        alertDialogValidate.setTitle("you lose !");
        alertDialogValidate.setMessage("Sorry you lose the game because you put a wrong answer :(");

    }

    protected void startChronometer(int duration)
    {
        time = duration;
        try{
            t.cancel();
        }
        catch (Exception e)
        {

        }
        t = new Timer();
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                //Called each time when 1000 milliseconds (1 second) (the period parameter)
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        time--;
                        textViewChronometer.setText("" + time);
                        if (time == 0)
                        {
                            try{
                                t.cancel();

                                alertDialog.show();
                                navigateToHome();

                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }

                });
            }};
        t.scheduleAtFixedRate(timerTask,0l,1000l);
    }

    public void navigateToHome() {
        // Navigate to game activity
        Intent i = new Intent(this, AnswersActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    public void navigateToHome(View view) {
        // Navigate to game activity
        alertDialogValidate.show();
        Intent i = new Intent(this, AnswersActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    protected void stopChronometer()
    {
        try{
            t.cancel();
        }
        catch (Exception e)
        {

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);


    }
    class MyDragListener implements View.OnDragListener {
        Drawable enterShape = getResources().getDrawable(R.drawable.shape_droptarget);
        Drawable normalShape = getResources().getDrawable(R.drawable.shape);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    if(v.getId() != findViewById(R.id.layout_base_row1).getId() && v.getId() != findViewById(R.id.layout_base_row2).getId() )
                        v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundDrawable(normalShape);
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    container.addView(view);
                    view.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundDrawable(normalShape);
                default:
                    break;
            }
            return true;
        }
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_game, container, false);
            return rootView;
        }
    }

}


