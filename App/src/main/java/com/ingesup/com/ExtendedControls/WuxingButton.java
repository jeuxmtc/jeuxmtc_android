package com.ingesup.com.ExtendedControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by fantastik78 on 05/12/2013.
 * Button control using the shangai.ttf font
 */
public class WuxingButton extends Button {

    public WuxingButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public WuxingButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WuxingButton(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "shanghai.ttf");
        setTypeface(tf);
    }

}