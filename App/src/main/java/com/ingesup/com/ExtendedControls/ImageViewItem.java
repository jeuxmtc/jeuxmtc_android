package com.ingesup.com.ExtendedControls;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Gally on 12/12/13.
 */
public class ImageViewItem extends ImageView {

    public int SolutionId;

    public ImageViewItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
