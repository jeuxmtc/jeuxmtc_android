package com.ingesup.com.ExtendedControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by fantastik78 on 05/12/2013.
 * TextView control using the shangai.ttf font
 */
public class WuxingTextView extends TextView {

    public WuxingTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public WuxingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WuxingTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "shanghai.ttf");
        setTypeface(tf);
    }

}