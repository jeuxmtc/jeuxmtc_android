package com.ingesup.com;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class AnswersActivity extends Activity {

    public ArrayList<String> explanations;
    public ArrayList<Integer> times;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_answers);

        times = new ArrayList<Integer>();
        times.add(1456);
        times.add(5321);

        explanations = new ArrayList<String>();
        explanations.add("Proin vel nulla mi. Ut eget egestas lacus. Vestibulum id dolor auctor, viverra elit quis, varius metus. Aliquam non lorem vel elit rutrum dapibus eu.");
        explanations.add("Duis libero nisi, gravida placerat erat sit amet, eleifend faucibus mauris. Aliquam leo sapien, dapibus a venenatis non, elementum eget ante. Praesent sem nisl, posuere.");

        ArrayList<HashMap<String, String>> content = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < this.times.size(); i++){
            HashMap<String, String> c = new HashMap<String, String>();
            c.put("question", String.format("Question %d -", i + 1));
            c.put("time", String.format("%dms", times.get(i)));
            c.put("explanation", explanations.get(i));

            content.add(c);
        }

        SimpleAdapter adapter = new SimpleAdapter(this.getBaseContext(), content, R.layout.answers_list_adapter,
                new String[] {"question", "time", "explanation"}, new int[] {R.id.question_label, R.id.time_label, R.id.explanation_label});

        ListView answersList = (ListView)findViewById(R.id.answers_listview);
        answersList.setAdapter(adapter);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    public void navigateToHome(View view) {
        // Navigate to game activity
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.answers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_answers, container, false);
            return rootView;
        }
    }

}
