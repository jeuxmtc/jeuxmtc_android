package com.ingesup.com.Tools;

import com.google.gson.Gson;
import com.ingesup.com.Model.Question;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Gabriel on 12/12/13.
 */
public class Json {

    public static JSONObject obj = null;
    public static Thread thread = null;

    public static void getJson(final String url){
        thread = new Thread(new Runnable() {
            public void run() {
                getJsonMethod(url);
            }
        });
        thread.start();
    }

    private static void getJsonMethod(String url) {
        InputStream is = null;
        String result = "";
        JSONObject jsonObject = null;

        // HTTP
        InputStream content = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet(url));
            content = response.getEntity().getContent();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Read response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(content,"utf-8"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            content.close();
            result = sb.toString();
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        // Convert string to object
        try{
            Gson gson = new Gson();
            Question[] questions = gson.fromJson(result, Question[].class);
            Question.questions = questions;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}

